import java.util.HashSet;
import java.util.TreeSet;
import java.util.Set;


public class Main {

        public static void main(String[] args) {
            Set<String> cohort1 = new HashSet<String>();
            cohort1.add("United States");
            cohort1.add("United States");
            cohort1.add("Ukraine");
            cohort1.add("Mexico");

            System.out.println("Cohort1 List:" +cohort1);


            Set<String> cohort2 = new HashSet<String>();
            cohort2.add("United States");
            cohort2.add("Canada");
            cohort2.add("United States");
            cohort2.add("Mexico");
            System.out.println("Cohort2 List:"+ cohort2);

            Set<String> cohort3 = new HashSet<String>(cohort1);
            cohort3.addAll(cohort2);
            System.out.println("Cohort1 & Cohort2 List:"+ cohort3);


            Set<String> cohort4 = new TreeSet<String>(cohort3);
            System.out.println("Cohort1 & Cohort2 List in alphabetical:" + cohort4);


            Set<String> setIntersection = new HashSet<String>(cohort1);
            setIntersection.retainAll(cohort2);
            System.out.println("Common countries in cohort1 & cohort2 Lists:" + setIntersection);







        }
}
